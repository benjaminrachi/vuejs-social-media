import Vue from 'vue';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import {
    faUser,
    faBurn,
    faSignOutAlt,
    faThumbsUp,
    faComments,
} from '@fortawesome/free-solid-svg-icons'

library.add(
    faUser,
    faBurn,
    faSignOutAlt,
    faThumbsUp,
    faComments,
);

Vue.component('font-awesome-icon', FontAwesomeIcon); // registered globally