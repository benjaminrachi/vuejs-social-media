import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyDdLmaabssHXVlfotI1wjYyG61qTj2xt0A",
    authDomain: "vue-social-media-a9ac5.firebaseapp.com",
    databaseURL: "https://vue-social-media-a9ac5.firebaseio.com",
    projectId: "vue-social-media-a9ac5",
    storageBucket: "vue-social-media-a9ac5.appspot.com",
    messagingSenderId: "324715557070",
    appId: "1:324715557070:web:38cc7aea72f54b3700e09f"
};
firebase.initializeApp(config)

// firebase utils
const db = firebase.firestore()
const auth = firebase.auth();

const getCurrentUser = () => {
    return new Promise((resolve, reject) => {
        const unsubscribe = auth.onAuthStateChanged(user => {
            unsubscribe();
            resolve(user);
        }, reject);
    })
};

const saveDoc = (collectionName, docId, doc) => {
    if (docId) {
        return db.collection(collectionName)
            .doc(docId)
            .set(doc, { merge: true })
    }
    else {
        return db.collection(collectionName)
            .add(doc)
    }
};

export {
    db,
    auth,
    getCurrentUser,
    saveDoc,
};