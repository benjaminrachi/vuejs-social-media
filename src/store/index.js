import Vue from 'vue'
import Vuex from 'vuex'
import * as fb from '@/firebase.config.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null,
    posts: [],
    hiddenPosts: [],
  },
  getters: {
    user: state => state.user,
    isLoggedIn: state => !!state.user,
  },
  mutations: {
    SET_USER(state, user) {
      state.user = user;
    },
    LOG_OUT(state) {
      state.user = null;
      state.posts = [];
      state.hiddenPosts = [];
    },
    SET_POSTS(state, posts) {
      state.posts = posts;
    },
    SET_HIDDEN_POSTS(state, hiddenPosts) {
      state.hiddenPosts = hiddenPosts;
    },
    LIKE_POST(state) {
    },
    COMMENT_POST(state) {
    }

  },
  actions: {
    async setUser({ commit, dispatch }, user) {
      await fb.saveDoc('users', user.uid, user)
      commit('SET_USER', user);
      await dispatch('fetchPosts');
    },
    async signOut({ commit }) {
      await fb.auth.signOut()
      commit('LOG_OUT');
    },
    async createPost({ commit }, post) {
      const res = await fb.saveDoc('posts', null, post);
    },
    async fetchPosts({ commit }) {
      fb.db.collection('posts').orderBy('createdOn', 'desc')
        .onSnapshot((postCollectionSnapshot) => {

          if (postCollectionSnapshot.docChanges().length !== postCollectionSnapshot.docs.length &&
            postCollectionSnapshot.docChanges()[0].type === "added" &&
            postCollectionSnapshot.docChanges()[0].doc.data().userId !== this.state.user.uid) {

            let hiddenPost = { ...postCollectionSnapshot.docChanges()[0].doc.data(), id: postCollectionSnapshot.docChanges()[0].doc.id }
            commit('SET_HIDDEN_POSTS', [hiddenPost, ...this.state.hiddenPosts]);
          }

          else {
            let posts = [];
            postCollectionSnapshot.forEach(post => {
              posts.push({ ...post.data(), id: post.id })
            });
            commit('SET_POSTS', posts);
          }

        });
    },
    showHiddenPosts({ commit }) {
      commit('SET_POSTS', [...this.state.hiddenPosts, ...this.state.posts]);
      commit('SET_HIDDEN_POSTS', []);
    },
    async likePost({ commit }, post) {
      const docId = `${this.state.user.uid}_${post.id}`;

      const existingLike = await fb.db.collection('likes').doc(docId).get();

      //the user has already liked this post
      if (existingLike.exists) return;

      await fb.saveDoc('likes', docId, { postId: post.id, userId: this.state.user.uid });
      await fb.saveDoc('posts', post.id, { ...post, likesCounter: ++post.likesCounter });
      commit('LIKE_POST');
    },
    async createComment({ commit }, { comment, post }) {
      await fb.saveDoc('comments', null, comment);
      await fb.saveDoc('posts', comment.postId, { ...post, commentsCounter: ++post.commentsCounter })
      commit('COMMENT_POST');
    },
    async fetchPostComments({ commit }, postId) {
      const postComments = await fb.db.collection('comments').where('postId', '==', postId).get()
      debugger;
    }
  },
  modules: {
  }
})
