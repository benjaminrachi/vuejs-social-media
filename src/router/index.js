import Vue from 'vue'
import VueRouter from 'vue-router'
import * as fb from '@/firebase.config.js'
import Dashboard from '@/views/Dashboard'
import Auth from '@/views/Auth'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'dashboard',
    component: Dashboard,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/auth',
    name: 'auth',
    component: Auth
  },
  // {
  //   path: '*',
  //   redirect: '/dashboard'
  // },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach(async (to, from, next) => {
  const requiresAuth = to.matched.some(x => x.meta.requiresAuth);
  const currentUser = await fb.getCurrentUser();
  if (requiresAuth && !currentUser) {
    next("/auth");
  }
  else {
    next();
  }
})

export default router
