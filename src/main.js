import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/styles/app.scss'
import * as fb from '@/firebase.config.js'
import '@/font-awesome.config'
import moment from "moment";
import AsyncComputed from 'vue-async-computed'

Vue.use(AsyncComputed)

Vue.config.productionTip = false

Vue.filter('formatDate', function (val) {
  if (!val) {
    return "-";
  }
  let date = val.toDate();
  return moment(date).fromNow();
});

Vue.filter('trimLength', function (val) {
  if (val.length < 200) {
    return val;
  }
  return `${val.substring(0, 200)}...`;
});

new Vue({
  router,
  store,
  created() {
    fb.auth.onAuthStateChanged(async (userFireBase) => {
      if (!userFireBase) return;
      let user = {
        uid: userFireBase.uid,
        name: userFireBase.displayName,
        email: userFireBase.email,
        photoURL: userFireBase.photoURL,
      };

      await this.$store.dispatch('setUser', user);
    });
  },
  render: h => h(App)
}).$mount('#app')
